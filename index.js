console.log("Hello World");

/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

    function printWelcomeMessage(){
        let fullName = prompt("What is your name?");
        let age = prompt("How old are you?");
        let address = prompt("Where do you live?");
        alert("Thank you for your input!");
        console.log(fullName);
        console.log(age);
        console.log(address);
    }
    printWelcomeMessage();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/

	//second function here:
    function myTopFiveBands(){
        let topOne = "The Beatles";
        let topTwo = "Metallica";
        let topThree = "The Eagles";
        let topFour = "L'Arc-en-Ciel";
        let topFive = "Eraserheads";
        console.log("1. " + topOne);
        console.log("2. " + topTwo);
        console.log("3. " + topThree);
        console.log("4. " + topFour);
        console.log("5. " + topFive);
    }
    myTopFiveBands();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

    function myTopFiveMovies(){
        let topOne = "The Godfather";
        let topTwo = "The Godfather, Part II";
        let topThree = "Shawshank Redemption";
        let topFour = "To Kill a Mockingbird";
        let topFive = "Psycho";
        let rating = "Rotten Tomatoes Rating:";
        console.log("1. " + topOne);
        topOne = "97%";
        console.log(rating + " " + topOne);
        console.log("2. " + topTwo);
        topTwo = "96%";
        console.log(rating + " " + topTwo);
        console.log("3. " + topThree);
        topThree = "91%";
        console.log(rating + " " + topThree);
        console.log("4. " + topFour);
        topFour = "93%";
        console.log(rating + " " + topFour);
        console.log("5. " + topFive);
        topFive = "96%";
        console.log(rating + " " + topFive);
    }
    myTopFiveMovies();

// Another Version using Arrays
/*
    function myTopFiveMovies(){
        let topMovies = ["The Godfather", "The Godfather, Part II", "Shawshank Redemption", "To Kill a Mockingbird", "Psycho"];
        let topRating = [97, 96, 91, 93, 96];
        let rating = "Rotten Tomatoes Rating:";
        console.log("1. " + topMovies[0]);
        console.log(rating + " " + topRating[0] + "%");

        console.log("2. " + topMovies[1]);
        console.log(rating + " " + topRating[1] + "%");

        console.log("3. " + topMovies[2]);
        console.log(rating + " " + topRating[2] + "%");

        console.log("4. " + topMovies[3]);
        console.log(rating + " " + topRating[3] + "%");

        console.log("5. " + topMovies[4]);
        console.log(rating + " " + topRating[4] + "%");
    }
    myTopFiveMovies();
*/



/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/



// Debugging Part
// printUsers();


let printFriends/*printFriends()*/ = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt/*alert*/("Enter your first friend's name:"); 
	let friend2 = prompt/*prom*/("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3/*friends*/); 
};
printFriends();
/*
console.log(friend1); not needed
console.log(friend2); not needed
*/